(define-module (sxml xmlish)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (sxml xmlish simplish)
  #:export (define-xml-names
	     xml->xmlish
	     xmlish->xml
	     xmlish->stx))

(define (pp x)
  (pretty-print x)
  x)

(define translators (make-weak-key-hash-table))

(define (nm->fkn nm ns)
  (let ((dir (resolve-module ns)))
    (if (module-defined? dir nm)
	(module-ref dir nm)
	#f)))

(define (nm? nm ns)
  (hash-ref translators (nm->fkn nm ns) #f))

(define (register nm class x)
  (let ((m (make-hash-table)))
    (hash-set! translators
	       (module-ref (current-module) nm)
	       (cons class m))
    (for-each 
     (lambda (x)
       (match x
	 ((n) #f)	 
	 ((#('xmlish (nm ns) _) f1 f2)
	  (hash-set! m (list nm ns) (cons f1 f2)))
	 ((n f1 f2)
	  (hash-set! m (list n #f)  (cons f1 f2)))))
     x)))

(define (mk-stx fkn var)
  (lambda (x)
    (define fs (syntax-source x))
    (define as #f)
    (syntax-case x (@)
      ((_  (@ aa ...) . l)
       (begin
	 (set! as (map (lambda (x) 
			 (d->syntax (list 'quote (syntax-source x))))
		       #'(aa ...)))
	 #t))
      (_ #t))

    (syntax-case x (@)
      ((_  (@ (aa  vv) ...) . l)
       (with-syntax (((as ...) as))
	  #`(#,fkn 
	     #,(d->syntax `(quote ,fs)) 
	     (list '@ (list aa vv as) ...) . l)))
      ((_  . l)
       #`(#,fkn #,(d->syntax `(quote ,fs)) (list '@) . l))
      (_
       var))))
	  	  
(define-syntax define-xml-name
  (lambda (x)
    (define (gen nm v)
      (datum->syntax 
       nm
       (gensym
	(string-append
	 (symbol->string
	  (syntax->datum nm))
	 v))))
       
    (syntax-case x ()
      ((_ (nm (a . v) ...))
       #'(define-xml-name (nm #f (a . v) ...)))

      ((_ (nm class (a . v) ...))
       (with-syntax ((var (gen #'nm "-var"))
		     (fkn (gen #'nm "-fkn")))
	  #'(begin
	      (define var (vector 'xmlish 
				  (list 'nm (module-name (current-module)))
				  '()))
	      (define fkn
		(lambda (src . x) (list* (cons var src) x)))

	      (define-syntax nm (mk-stx #'fkn #'var))

	      (register 'nm class (list (list a . v) ...)))))
      
      ((_ nm)
       #'(define-xml-name (nm #f))))))


(define-syntax define-xml-names
  (syntax-rules ()
    ((define-xml-names prefix  l ...)
     (begin
       (define namespace-prefix (symbol->string 'prefix))
       (export namespace-prefix)
       (define-xml-name l)
       ...))))

(define (translate nm ns anm ans nmap car v)
  (let ((a (hash-ref translators (nm->fkn nm ns) #f)))
    (if a
	(let ((a (cdr a)))
	  (let ((tr (hash-ref a (list anm ans) #f)))
	    (if tr
		((car tr) nmap v)
		v)))
	v)))

(define (xml->xmlish . l)	     
  (let lp ((x (match (apply xml->sxml (append l (list #:xmlish #t)))
		 ((*TOP* PI y) y))))
    (match x 
      (((and f #('xmlish (nm ns) nmap)) 
	('@ ((and a #('xmlish (anm ans) _)) v) ...) u ...)
       (list* f
	      (list* '@ (map (lambda (na anm ans v)
			       (if ans
				   (list na
					 (translate nm ns
						    anm ans nmap car v))
				   (list anm
					 (translate nm ns
						    anm ans nmap car v))))
			     a anm ans v))
	      (map lp u)))
      (((and f #('xmlish (nm ns) nmap)) u ...)
       (list* f
	      (list '@)
	      (map lp u)))
      (x x))))
	     
       

(define (xmlish->xml x)
  (define tr (make-hash-table))

  (define (mkns ns)
    (string-join (map symbol->string ns) "/"))

  (define (mktag nm ns-str)
    (string->symbol
     (string-append ns-str ":" (symbol->string nm))))

  (define (get nm ns)
    (let ((m (hash-ref tr ns #f)))
      (if m
	  (mktag nm m)
	  (let ((m (mkns ns)))
	    (hash-set! tr ns m)
	    (mktag nm m)))))

  (define prefix-table (make-hash-table))
  (define translation-table (make-hash-table))
  (define prefixes '())
  (define n 0)
  (define (get-new-prefix)
    (set! n (+ n 1))
    (string-append "n" (number->string n)))
  (define (get-prefix l)
    (module-ref (resolve-module l) 'namespace-prefix))
  (define (mk-xmlns x s)
    (list (string->symbol (string-append "xmlns:" x)) s))
  (define (gen-nm nm ns)
    (let ((prefix (hash-ref translation-table ns #f)))
      (string->symbol
       (string-append prefix 
		      ":" 
		      (symbol->string nm)))))

  ;; Collect information about namespaces
  (let loop ((x x))
    (match x
      (((x . l) . u)
       (loop (cons x u)))

      ((#('xmlish (nm ns) nmap) ('@ (la . lv) ...) . u)
       (get nm ns)
       (for-each 
	(lambda (a) 
	  (let lp ((a a))
	    (match a
	       ((x y src)
		(lp (list x y)))
	       (#('xmlish (nm ns) nmap)
		(get nm ns))
	       (_ #f))))
	la)
       (for-each loop u))
      (x x)))
  
  ;; Deduce namespace prefixes
  (hash-for-each
   (lambda (l s)
     (let loop ((prefix (get-prefix l)))
       (if (hashq-ref prefix-table prefix #f)
	   (loop (get-new-prefix))
	   (let ((a (mk-xmlns prefix (string-append "http://" s))))
	     (hashq-set! prefix-table prefix a)
	     (hash-set! translation-table l prefix)
	     (set! prefixes (cons a prefixes))))))
   tr)
     
  
  (sxml->xml
   `(*TOP* 
     (*PI* xml ,(format #f "version=\"1.0\" encoding=\"ISO-8859-1\""))
     ,(let lp ((x x) (first #t))
	(define (loop x) (lp x #f))

	(match x
	  (((x . l) . u)
	   (lp (cons x u) first))

	  ((#('xmlish (nm ns) nmap) ('@ . l) . u)
	   `(,(gen-nm nm ns) (@ 
			      ,@(if first
				    prefixes
				    '())
			      ,@(map 
				 (lambda (a)
				   (let lp ((a a))
				     (match a
					((x y src)
					 (lp (list x y)))
					
					((#('xmlish (anm ans) nmap) v)
					 (list 
					  (gen-nm nm ns)
					  (translate nm ns anm ans
						     nmap cdr v)))
					      
					(((? symbol? s) v)
					 (list 
					  s
					  (translate nm ns s #f
						     nmap cdr v))))))
				 l))
	     ,@(map loop u)))

	  (x x))))))


(define (nm->syntax x)
  (match x
    (#('xmlish (nm ns) _)
     (vector 'syntax-object nm '((top)) `(hygiene ,@ns)))
    (x x)))

(define (d->syntax d)
  (vector 'syntax-object d '((top)) '(hygiene guile-user)))

(define (xmlish->stx x)
  (define (mk-attr x src tr)
    (match x
      ((a b src2)
       (if src2
	   (mk-attr (list a b) src2 tr)
	   (mk-attr (list a b) src  tr)))
      (((? symbol? s) v)
       (let ((res (list
		   (d->syntax `(quote ,s))
		   v
		   (d->syntax tr))))
	 (set-source-properties! res src)
	 res))

      (((and a #('xmlish (nm ns) tr))  v)
       (let* ((na  (nm->syntax a))
	      (res (list na
			 v
			 (d->syntax tr))))
	 (set-source-properties! res src)
	 res))))

  (let lp ((x x) (src #f))
    (match x
      (((f . src2) . u)
       (if src2
	   (lp (cons f u) src2)
	   (lp (cons f u) src)))

      (((and f #('xmlish _ tr)) ('@ . l) . u)
       (let* ((as  (map (lambda (x) (mk-attr x src tr)) l))
	      (us  (map (lambda (x) (lp x src)) u))
	      (nm  (nm->syntax f))
	      (res (list* nm (list* (d->syntax '@) as) us)))
	 (set-source-properties! res src)
	 res))
      (x x))))
	 