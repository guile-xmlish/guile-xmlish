Mostly GNU Lesser General Public License (LGPL) version 2

But this uses some modified guile code e.g. module/sxml/xmlish/simplish.xml
And the license for that file is indicated by the header of that file.

See "COPYING.LIB" for more information about the overall license.
